package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    
    @Test
    public void testEcho(){

        assertEquals("Testing if the given number is equal to 5",5,App.echo(5));

    }
    @Test
    public void testOneMore(){

        int x = 5;
        assertEquals("Testing if the given number is equal to 6",x+1,App.oneMore(x));
    }
}
